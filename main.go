package main

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
)

type stringTransformFunc func(string) string

type stringTransform struct {
	transformFunc stringTransformFunc
}

func (st *stringTransform) handleTransform(s string) error {
	newS := st.transformFunc(s)
	fmt.Println("transformed str: ", newS)
	return nil
}

func transformHasSha256(s string) string {
	hash := sha256.Sum256([]byte(s))
	return hex.EncodeToString(hash[:])
}

func transformPrefix(prefix string) stringTransformFunc {
	return func(s string) string {
		return prefix + s
	}
}

func transformSuffix(suffix string) stringTransformFunc {
	return func(s string) string {
		return s + suffix
	}
}

func main() {
	// compose string transform to use prefix function
	sTPrefixItem := &stringTransform{
		transformFunc: transformPrefix("item_"),
	}
	sTPrefixItem.handleTransform("leather armour")

	// compose string transform to user sha function
	sTHash := &stringTransform{
		transformFunc: transformHasSha256,
	}
	sTHash.handleTransform("leather armour")

	// compose strung transform to use suffix
	sTSuffix := &stringTransform{
		transformFunc: transformSuffix("_item"),
	}
	sTSuffix.handleTransform("leather armour")
}
